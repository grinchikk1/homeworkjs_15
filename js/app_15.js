"use strict";

const factorialNumber = (n) => {
  if (n <= 1) {
    return 1;
  } else {
    return n * factorialNumber(n - 1);
  }
};

let userInput;

do {
  userInput = prompt("Введіть число:");
} while (isNaN(userInput) || userInput === "");

const num = Number(userInput);
const result = factorialNumber(num);

alert(`Факторіал числа ${num} дорівнює ${result}`);
